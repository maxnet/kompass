//
// SPDX-FileCopyrightText: 2021 Sylvain Migaud <sylvain@migaud.net>
//
// SPDX-License-Identifier: GPL-2.0-or-later
//

import QtQuick 2.7
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2

import org.kde.kompass 1.0

Kirigami.ApplicationWindow {
    id: root
    title: "Kompass"

    Compass {
        id: compass
    }

    Kirigami.InlineMessage {
        text: i18n("No compass hardware available")
        visible: !compass.available
        anchors.centerIn: parent
        width: Kirigami.Units.gridUnit * 20
    }

    ColumnLayout {
        width: root.width
        height: root.height
        spacing: 10

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Image {
                id: imagePhoto
                source: "qrc:/kompassHand.png"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width > parent.height ? parent.height : parent.width
                height: parent.width > parent.height ? parent.height : parent.width
                transformOrigin: Item.Center
                rotation: compass.rotation
                visible: compass.available
            }
        }
    }
}
